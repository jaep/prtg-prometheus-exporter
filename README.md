# PRTG exporter for Prometheus

This simple exporter exposes PRTG sensors as Prometheus metrics.

## Pre-requisites

In order to configure your exporter, you'll need to retrieve you password hash to connect to PRTG API.
At EPFL, this has can be retrieved by retrieving the following url:

```bash
curl https://dc-mon-core1.epfl.ch/api/getpasshash.htm?username=<GASPAR_USERNAME>&password=<GASPAR_PASSWORD>
```

## build process

### Docker

The easiest way to use it is to build it then run it:

```bash
docker-compose -f dev.docker-compose.yml build
```

Then push to your favorite registry:

```bash
docker tag ic-it-prtg-exporter:latest ic-registry.epfl.ch/ic-it/ic-it-prtg-exporter:latest
docker push ic-registry.epfl.ch/ic-it/ic-it-prtg-exporter:latest
```

### Local build

As usual with python, you should use a virtualenv:

```bash
python -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## Usage

It will start a fastapi server on port 8000 by default.

You can simply run it with:

```bash
python main.py
```

Then open the browser on http://localhost:8000
