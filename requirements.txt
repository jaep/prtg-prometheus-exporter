fastapi==0.85.1
Jinja2==3.1.2
python-decouple==3.6
redis==4.3.4
requests==2.28.1
uvicorn==0.19.0
