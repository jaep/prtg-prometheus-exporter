from typing import List

from jinja2 import Environment, FileSystemLoader, select_autoescape


def prepare_data(sensors_data: List[dict]) -> str:
    env = Environment(loader=FileSystemLoader([".", "src"]), autoescape=select_autoescape())

    template = env.get_template("metrics.txt.j2")

    return_value = template.render(data=sensors_data)
    return return_value
