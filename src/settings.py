from decouple import config

PRTG_BASE_URL = config("PRTG_BASE_URL")
PRTG_USERNAME = config("PRTG_USERNAME")
PRTG_PASSWORD_HASH = config("PRTG_PASSWORD_HASH")
REDIS_HOST = config("REDIS_HOST", default="redis")
REDIS_PORT = config("REDIS_PORT", default=6379, cast=int)
