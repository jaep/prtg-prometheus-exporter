import logging
import xml.etree.ElementTree as ET
from typing import List

import requests
import urllib3
from requests.exceptions import ConnectTimeout, Timeout

import sensors
import settings

logger = logging.getLogger(__name__)


def get_all_sensors_data() -> List[dict]:
    logger.info("Getting all sensors data")

    return_value = []
    for sensor in sensors.SENSORS:
        value, unit = get_sensor_data(sensor)
        sensor["instant_consumption"] = value
        sensor["consumption_unit"] = unit
        return_value.append(sensor)

    return return_value


def get_sensor_data(sensor: dict) -> dict:

    logger.info(f"Getting data for sensor {sensor['id']}")

    url = f"{settings.PRTG_BASE_URL}/api/getobjectstatus.htm"
    payload = {
        "username": settings.PRTG_USERNAME,
        "passhash": settings.PRTG_PASSWORD_HASH,
        "id": sensor["id"],
        "show": "nohtmlencode",
        "name": "lastvalue",
    }

    try:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        response = requests.get(url, params=payload, verify=False, timeout=5)
    except (ConnectTimeout, Timeout):
        logger.warning(f"Timeout getting data for sensor {sensor['id']}")
        return None

    if response.status_code != 200:
        logger.warning(
            f"Error getting data for sensor {sensor['id']}, status code: {response.status_code}"
        )
        return None

    root = ET.fromstring(response.content)
    element = root.find(".//result")
    element_text = element.text
    element_array = element_text.split(" ")
    if len(element_array) == 2:
        value = float(element_array[0].replace(",", "."))
        unit = element_array[1]
    elif len(element_array) == 3:
        value = float(f"{element_array[0]}{element_array[1]}".replace(",", "."))
        unit = element_array[2]
    else:
        logger.warning(f"Error parsing data for sensor {sensor['id']}")
        return None

    # Make sure that we have the same unit for all sensors
    if unit == "Watt":
        value = value / 1000
        unit = "kW"

    return value, unit
