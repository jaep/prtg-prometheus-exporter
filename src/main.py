import logging
import sys
from email.header import Header

import redis
import uvicorn
from fastapi import FastAPI, Request, Response
from fastapi.responses import PlainTextResponse

import prometheus
import prtg
import settings

logger = logging.getLogger(__name__)

app = FastAPI()

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT)


@app.get("/metrics", response_class=PlainTextResponse)
async def get_data():
    headers = {"Content-Type": "text/plain"}
    metrics_page = r.get("prometheus_prtg_exporter_metrics")
    if metrics_page is None:
        headers["x-cache"] = "MISS"
        values = prtg.get_all_sensors_data()
        metrics_page = prometheus.prepare_data(values)
        r.set("prometheus_prtg_exporter_metrics", metrics_page, ex=15)
    else:
        headers["x-cache"] = "HIT"

    return PlainTextResponse(content=metrics_page, headers=headers)


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        stream=sys.stdout,
    )
    logger.info("Starting PRTG Prometheus Exporter")
    uvicorn.run(app, host="0.0.0.0", port=80)
